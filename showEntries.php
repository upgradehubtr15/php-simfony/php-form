<!DOCTYPE html>
<html>
    <head>
        <title>DataEntries</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="styles.css" />
    </head>
    <body>
        <div class="container">
            <header class="header">
                <a href="./index.php" class="index">Index</a>
                <a href="./showEntries.php">Data Entries</a>
                <a href="./search.php">Search</a>
            </header>
            
            <h2 class="text-center">DataEntries</h2>
            <div class="cabecera">
                <div class="col-md-2 dataRow">
                    ID                            
                </div>  
                <div class="col-md-2 dataRow">
                    UserName                            
                </div>  
                <div class="col-md-2 dataRow">
                    Password                            
                </div>  
                <div class="col-md-2 dataRow">
                    FirstName                            
                </div>  
                <div class="col-md-2 dataRow">
                    LastName                            
                </div>  
                <div class="col-md-2 dataRow">
                Date of Birth                            
                </div>  
            </div>
            <br />
            <?php
                if (($handle = fopen("contact_data.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, ",")) !== FALSE) {
                    $num = count($data);
                    for ($i=0; $i < $num; $i++) {
                        ?>
                        <div class="col-md-2 dataRow">
                        <?php    
                            echo $data[$i] . "<br />";
                        ?>
                        </div>  
                        <?php    
                    }
                }
                fclose($handle);
                }
            ?>        
        </div>
    </body>
</html>

