<!DOCTYPE html>
<html>
    <head>
        <title>Search</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./styles.css" />
    </head>
    <body>
        <div class="container">
            <header class="header">
                <a href="./index.php" class="index">Index</a>
                <a href="./showEntries.php">Data Entries</a>
                <a href="./search.php">Search</a>
                <form method="post" action="search.php" class="searchForm">
                    <input type="text" name="searched" class="searched">
                    <input type="submit" value="Search" name="submit" class="btn-primary"> <!-- assign a name for the button -->
                </form>
            </header>

            <h2 class="text-center">Search it</h2>
            <div class="cabecera">
                <div class="col-md-2 dataRow">
                    ID                            
                </div>  
                <div class="col-md-2 dataRow">
                    UserName                            
                </div>  
                <div class="col-md-2 dataRow">
                    Password                            
                </div>  
                <div class="col-md-2 dataRow">
                    FirstName                            
                </div>  
                <div class="col-md-2 dataRow">
                    LastName                            
                </div>  
                <div class="col-md-2 dataRow">
                    Date of Birth                            
                </div>  
            </div>
            <br />
            <?php
                if(isset($_POST)){
                    $result  = [];
                    if (($handle = fopen("contact_data.csv", "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, ",")) !== FALSE) {
                            $num = count($data);
                            for ($i=0; $i < $num; $i++) {
                                if($data[$i] == $_POST['searched']) 
                                array_push($result, $data);
                            }
                        }
                        fclose($handle);
                    }
                    
                    foreach ($result as $clave=>$valor)
                    {
                        foreach ($valor as $clave=>$dato)
                        {
                            ?>
                        <div class="col-md-2 dataRow">
                        <?php    
                            echo $dato . "<br />";
                            ?>
                        </div>  
                        <?php    
                        }
                    }
                }
                ?>        
        </div>
    </body>
</html>

<?php

