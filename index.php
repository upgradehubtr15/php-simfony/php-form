<!DOCTYPE html>
<html>
    <head>
        <title>Formluario PHP</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./styles.css" />
    </head>
    <body>
        <div class="container">
            <header class="header">
                <a href="./index.php" class="index">Index</a>
                <a href="./showEntries.php">Data Entries</a>
                <a href="./search.php">Search</a>
            </header>

            <h2 class="text-center">Formulario PHP</h2>
            <br />
            <div class="col-md-6" style="margin:0 auto; float:none;">
                <form method="post">
                    <h3 class="text-center">Contact Form</h3>
                    <br />
                    <div class="form-group">
                        <label>Nombre de usuario</label>
                        <input type="text" name="userName" placeholder="Nombre de usuario" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label>Enter password</label>
                        <input type="text" name="password" class="form-control" placeholder="Enter password"  />
                    </div>
                    <div class="form-group">
                        <label>Enter firstName</label>
                        <input type="text" name="firstName" class="form-control" placeholder="Enter firstName" />
                    </div>
                    <div class="form-group">
                        <label>Enter lastName</label>
                        <input type="text" name="lastName" class="form-control" placeholder="Enter lastName" />
                    </div>
                    <div class="form-group">
                        <label>Enter dateOfBirth</label>
                        <input type="date" data-date="" data-date-format="DD MMMM YYYY"  name="dateOfBirth" class="form-control" />
                    </div>
                        <div class="form-group">
                        <input type="submit" name="submit" class="btn btn-info col-md-12" value="Submit" />
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
<?php

$name = '';
$password = '';
$firstName = '';
$lastName = '';
$dateOfBirth = '';

date_default_timezone_set('Europe/London');

function limpiarString($string)
{
    //eliminamos los espcios en blanco del principio y el final
    $string = trim($string);
    return $string;
}

if(isset($_POST["submit"]))
{
    $userName = limpiarString($_POST["userName"]);
    $password = limpiarString($_POST["password"]);
    $firstName = limpiarString($_POST["firstName"]);
    $lastName = limpiarString($_POST["lastName"]);
    $dateOfBirth = limpiarString($_POST["dateOfBirth"]);
    $register = fopen("contact_data.csv", "a");
    $id = count(file("contact_data.csv"));
    $id = $id + 1;
    $data = array(
    'id'  => "$id",
    'userName'  => "$userName",
    'password'  => $password,
    'firstName' => $firstName,
    'lastName' => $lastName,
    'dateOfBirth' => $dateOfBirth
    );
    
    fputcsv($register, $data);

    $userName = '';
    $password = '';
    $firstName = '';
    $lastName = '';
    $dateOfBirth = '';
 }

?>